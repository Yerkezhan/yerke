<?php
require 'db.php';
$sql = 'SELECT * FROM com';
$statement = $connection->prepare($sql);
$statement->execute();
$com = $statement->fetchAll(PDO::FETCH_OBJ);
 ?>
<?php require 'header.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>All comments </h2>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th>ID</th>
          <th>Comments</th>
          <th>Rate</th>
          <th>Action</th>
        </tr>
        <?php foreach($com as $person): ?>
          <tr>
            <td><?= $person->id; ?></td>
            <td><?= $person->cmt; ?></td>
            <td><?= $person->rate; ?></td>
            <td>
              <a href="edit.php?id=<?= $person->id ?>" class="btn btn-info">Edit</a>
              <a onclick="return confirm('Are you sure you want to delete this entry?')" href="delete.php?id=<?= $person->id ?>" class='btn btn-danger'>Delete</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
</div>
<?php require 'footer.php'; ?>
