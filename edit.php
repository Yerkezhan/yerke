<?php
require 'db.php';
$id = $_GET['id'];
$sql = 'SELECT * FROM com WHERE id=:id';
$statement = $connection->prepare($sql);
$statement->execute([':id' => $id ]);
$com = $statement->fetch(PDO::FETCH_OBJ);
if (isset ($_POST['cmt'])  && isset($_POST['rate']) ) {
  $cmt = $_POST['cmt'];
  $rate = $_POST['rate'];
  $sql = 'UPDATE com SET cmt=:cmt, rate=:rate WHERE id=:id';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':cmt' => $cmt, ':rate' => $rate, ':id' => $id])) {
    header("Location: index.php");
  }



}


 ?>
<?php require 'header.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Update comments</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
          <label for="cmt">cmt</label>
          <input value="<?php echo $person->cmt;?>" type="text" name="cmt" id="cmt" class="form-control">
        </div>
        <div class="form-group">
          <label for="rate">rate</label>
          <input type="text" value="<?php echo $person->rate;?>" name="rate" id="rate" class="form-control">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-info">Update comments</button>
                    <button type="hidden" class="btn btn-info">Update comments</button>

        </div>
      </form>
    </div>
  </div>
</div>
<?php require 'footer.php'; ?>
