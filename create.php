<?php
require 'db.php';
$message = '';
if(isset($_POST['Submit'])) { 

  $cmt = $_POST['cmt'];
  $rate = $_POST['rate'];
  $sql = 'INSERT INTO com(cmt,rate) VALUES(:cmt, :rate)';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':cmt' => $cmt, ':rate' => $rate])) {
    $message = 'data inserted successfully';
  }

}

 ?>
<?php require 'header.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Create a comment</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
          <label for="cmt">Comment</label>
          <input type="text" name="cmt" id="cmt" class="form-control">
        </div>
        <div class="form-group">
          <label for="rate">Rate</label>
          <input type="text" name="rate" id="rate" class="form-control">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-info" name="Submit" value="Submit">Create a comment</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php require 'footer.php'; ?>